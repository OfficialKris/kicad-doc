
== Introduction to Pcbnew

=== Initial configuration

When Pcbnew is run for the first time, if the global footprint table file `fp-lib-table` is not
found in the KiCad configuration folder then Pcbnew will ask how to create this file:

NOTE: TODO: add screenshot

NOTE: TODO: add instructions on what these options mean

The default footprint library table includes all of the standard footprint libraries that are
installed as part of KiCad.

=== The Pcbnew user interface
:experimental:

image::images/pcbnew_user_interface.png[width=1000,scaledwidth=70%]

The main Pcbnew user interface is shown above, with some key elements indicated:

1. Top toolbars (file management, zoom tools, editing tools)
2. Left toolbar (display options)
3. Message panel and status bar
4. Right toolbar (drawing and design tools)
5. Appearance panel
6. Selection filter panel

=== Navigating the editing canvas

The editing canvas is a view onto the board being designed.  You can pan and zoom to different
areas of the board, and also flip the view to show the board from the bottom.

By default, dragging with the middle or right mouse button will pan the canvas view and scrolling
the mouse wheel will zoom the view in or out.  You can change this behavior in the Mouse and
Touchpad section of the preferences (see Configuration and Customization for details).

=== Hotkeys

NOTE: TODO: Write this section

The hotkeys described in this manual use the key labels that appear on a standard PC keyboard.
On an Apple keyboard layout, use the kbd:[Cmd] key in place of kbd:[Ctrl], and the kbd:[Option] key
in place of kbd:[Alt].

NOTE: Many of the actions available through hotkeys are also available in context menus.  To access
the context menu, right-click in the editing canvas.  Different actions will be available depending
on what is selected or what tool is active.
