FROM registry.gitlab.com/kicad/kicad-ci/doc_containers:latest as doc-build-env

WORKDIR /src
COPY . .

RUN mkdir -p build
WORKDIR /src/build
RUN cmake -DBUILD_FORMATS="pdf" ../
RUN make

FROM scratch as output-image

COPY --from=doc-build-env /src/build/src /src
